/*
 * R.Schneider
 * GUI mit Swing
 * 08.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Controller
 */

import java.awt.*;
import java.awt.event.*;
//import java.applet.Applet;
//package awtkit;

public class Controller
{
	private View view;
	
	private Model model;
	
	//-----------------------
	
	public Controller()
	{
		view=new View(400,400);
		model=new Model();
	}
	
	//----------------
}
