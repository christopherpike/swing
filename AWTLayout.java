/*
 * R.Schneider
 * GUI mit Swing
 * 09.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * AWTLayout zum Testen
 */

import java.awt.*;
import java.awt.event.*;

//package awtkit;

public class AWTLayout extends Frame
{
	//-------

	private Color farbe=Color.BLUE;
	
	private Panel pnl=new Panel();
	
	private Label lblONE=new Label("");

	private Graphics g;

	//-----

	public AWTLayout()
	{
		super("AWTLayout");
		//Fenstergröße
		setSize(600,600);
		//Startpunkt
		setLocation(100,100);
		//Layout
		//r_CardLayout();
		//r_GridLayout();
		//r_GridBagLayout();
		//r_FlowLayout();
		r_BorderLayout();
		//addInhalte();
		g=getGraphics();
		//Fensterschliesser
		addWindowListener(new RWindowListener());
		addMouseListener(new RMouseListener());
		//Anzeigen
		setVisible(true);
	}

	//------

	private void addInhalte()
	{
		add(new TextField("Textfeld 1."));
		add(new Label("Beschriftung 1."));
		add(new TextField("Textfeld 1."));
		add(new Button("Button 1 Aufschrift."));

		pnl.setBackground(farbe);
		add(pnl);
		add(lblONE);
		//add()
		//Checkbox
		//CheckboxGroup
		//List
		//Choice
		//Menubar
		//Menu
		
		
	}

	public void r_CardLayout()
	{
		setLayout(new CardLayout());
		addInhalte();
	}

	public void r_FlowLayout()
	{
		setLayout(new FlowLayout());
		addInhalte();
	}

	public void r_BorderLayout()
	{
		setLayout(new GridLayout());
		addInhalte();
	}

	public void r_GridLayout()
	{
		setLayout(new GridLayout());
		addInhalte();
	}

	public void r_GridBagLayout()
	{
		setLayout(new GridBagLayout());
		addInhalte();
	}
	
	class RMouseListener extends MouseAdapter
	{
		@Override
		public void mousePressed(MouseEvent me)
		{
			System.out.println((int)me.getButton());
			//setVisible(false);
			//pnl.setBackground(Color.RED);
			//System.out.println("Farbe.");
			//pnl.repaint();
			lblONE=new Label("Hello");
			//g.update();
			repaint();
		}
		
		@Override
		public void mouseReleased(MouseEvent me)
		{
			setVisible(false);
			dispose();
			System.exit(0);
		}
	}
	
	

	class RWindowListener extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent we)
		{
			setVisible(false);
			dispose();
			System.exit(0);
		}
	}

	public static void main(String[]arguments)
	{
		AWTLayout al=new AWTLayout();
	}
}
