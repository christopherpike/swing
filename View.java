/*
 * R.Schneider
 * GUI mit Swing
 * 08.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * View
 */

import java.awt.*;
import java.awt.event.*;

//package swing;

public class View extends Frame
{
	private int x;
	
	//--------------------
	
	public View(int width,int height)
	{
		//Startpunkt
		setLocation(100,100);
		//Setze Fenstergr��e
		setSize(width,height);
		//Setze Grundlayout
		setLayout(new FlowLayout());
		//F�ge Inhalte hinzu
		//addInhalte();
		addObjekte();
		//Listener
		addWindowListener(new RWindowListener());
		addMouseListener(new RMouseListener());
		//Anzeigen
		setVisible(true);
	}
	
	
	//--------------------
	
	private void addInhalte()
	{
		Graphics g=getGraphics();
		//Panel Left
		Panel pnlLeft=new Panel();
		//Panel Left Background
		pnlLeft.setBackground(Color.LIGHT_GRAY);
		//Layout
		
		//Zahl auf Panel Left
		TextField txtZahlPnlLeft=new TextField("8");
		//Nenner auf Panel Left
		TextField txtNennerPnlLeft=new TextField("1");
		//Teiler auf Panel Left
		TextField txtTeilerPnlLeft=new TextField("10");
		//Label Strich
		Label lblPnlLeft=new Label("-");
		
		//adden
		pnlLeft.add(txtZahlPnlLeft);
		pnlLeft.add(txtNennerPnlLeft);
		pnlLeft.add(lblPnlLeft);
		pnlLeft.add(txtTeilerPnlLeft);

		//Panel Mitte
		Panel pnlMitte=new Panel();
		//Panel Mitte Background
		pnlMitte.setBackground(Color.GRAY);
		//Label
		Label lblPnlMitte=new Label("-");
		
		//adden
		pnlMitte.add(lblPnlMitte);
		
		//Panel Right   https://www.youtube.com/watch?v=Q2asN30JC3M
		Panel pnlRight=new Panel();
		//Panel Right Background
		pnlRight.setBackground(Color.DARK_GRAY);
		
		
		//Zahl auf Panel Left
		TextField txtZahlPnlRight=new TextField("6");
		//Nenner auf Panel Left
		TextField txtNennerPnlRight=new TextField("2");
		//Teiler auf Panel Left
		TextField txtTeilerPnlRight=new TextField("4");
		//Label Strich
		Label lblPnlRight=new Label("-");

		//adden
		pnlRight.add(txtZahlPnlRight);
		pnlRight.add(txtNennerPnlRight);
		pnlRight.add(lblPnlRight);
		//
		pnlRight.add(txtTeilerPnlRight);
		//setColor(Color.RED);
		//g.drawRoundRect(10,10,50,50,5,5);
		//pnlRight.add(g.drawRoundRect(10,10,50,50,5,5));
		
		//Adden auf Hauptfenster
		add(pnlLeft);
		add(pnlMitte);
		add(pnlRight);
	}
	
	public void addObjekte()
	{
		//Graphics g=getGraphics();
		g.drawRoundRect(10,10,50,50,5,5);
		g.drawRect(10,10,50,50);
		
	}
	
	
	
	
	
	class RWindowListener extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent we)
		{
			setVisible(false);
			dispose();
			System.exit(0);
		}
	}
	
	class RMouseListener extends MouseAdapter
	{
		@Override
		public void mousePressed(MouseEvent me)
		{
			
		}
	}
}
